#!/usr/bin/env bash

BASEDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd "$BASEDIR"

. $BASEDIR/utils.sh

mvn clean install
assertReturnStatus $? "Failed building project"
#./mavenBuildSync.sh

echo Copying war to the server...
scp -i ~/.ssh/CarReport.pem target/server.war ubuntu@35.162.156.142:~/apache-tomcat-8.5.8/webapps/.
assertReturnStatus $? "Failed copying app to server"

#echo Deploying app on the server...
#cat amazonLocalRestart.sh | ssh -q -i ~/.ssh/CarReport.pem ubuntu@35.162.156.142
#assertReturnStatus $? "Failed Deploying app on the server"
