#!/usr/bin/env bash

BASEDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
#BASEDIR=$(dirname "$BASH_SOURCE")
#echo BASEDIR=$BASEDIR

. $BASEDIR/utils.sh

cd "$BASEDIR"

mvn clean install
assertReturnStatus $? "Failed building project"

echo Deploying to local server...
cp -f $BASEDIR/target/server.war $BASEDIR/apache-tomcat-8.5.8/webapps/.