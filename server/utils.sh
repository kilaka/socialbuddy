#!/usr/bin/env bash

function assertReturnStatus() { #params(returnStatus, message)
	if [ $1 -ne 0 ]
	then
		if [ ! -z "$2" ]
		then
			echo ERROR! Status: $1. Message: $2.
		fi
		# Disable to let the script finish
		# if [ "$INITIATING_SCRIPT" == "${0##*/}" ]
		# then
		# 	read -n1 -r -p "Press any key to continue..."
		# fi
		exit $1
	fi
}


shopt -s expand_aliases

alias rm='echo pleas do not use rm. Use trash instead!'
alias trash='NEW_DIR=~/.trash/`date +%s`; mkdir -p $NEW_DIR; /bin/mv --target-directory=$NEW_DIR $*'
