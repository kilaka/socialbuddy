package socialbuddy;

/**
 * Created by noa on 29/03/17.
 */
public class BigResponse {
    EmotionState realTime;
    ResponseMerger.TextToEmotionResponse history;

    public BigResponse() {
    }

    public EmotionState getRealTime() {
        return realTime;
    }

    public BigResponse setRealTime(EmotionState realTime) {
        this.realTime = realTime;
        return this;
    }

    public ResponseMerger.TextToEmotionResponse getHistory() {
        return history;
    }

    public void setHistory(ResponseMerger.TextToEmotionResponse history) {
        this.history = history;
    }
}
