package socialbuddy;

import com.google.common.io.Files;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import socialbuddy.speech2text.EmotionalStateWithText;
import socialbuddy.speech2text.Speech2Text;
import socialbuddy.synesketch.app.SlimEmotionalState;


/**
 * Created by alik on 11/21/16.
 */

@Singleton // Load the service only once.
@Path("/api")
public class ApiResource {

	public ApiResource() {
//		final Application application = new ResourceConfig().packages("org.glassfish.jersey.examples.multipart").register(MultiPartFeature.class);
	}

	//	Speech2Text speech2Text = getSpeech2Text();
	SlimEmotionalState lastSlimEmotionalState = null;
	Gson gson = new GsonBuilder()
			.setPrettyPrinting()
			.disableHtmlEscaping()
			.create();
	private long lastTimeForTextToSpeech = 0;

	private static Speech2Text getSpeech2Text() {
		try {
			return new Speech2Text();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("exception creating speech2Text ", e);//todo: // FIXME: 29/03/17
		}
	}

	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
//	@Consumes(MediaType.APPLICATION_OCTET_STREAM)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/process")
	public String process(
//			byte[] binaryInput
//			InputStream inputStream
//			@Multipart(value="stream", type="application/octet-stream")  InputStream inputStream
			@FormDataParam("file") InputStream inputStream
			, @FormDataParam("file") FormDataContentDisposition cdh
//			@FormDataParam("file") FormDataBodyPart filePart
	) throws Exception {
//		InputStream inputStream = (InputStream) filePart.getEntity();
		// Create JAX-RS application.
//		return "Output of input: " + IOUtils.toString(inputStream, StandardCharsets.UTF_8);
		byte[] amrAsBytes = IOUtils.toByteArray(inputStream);

		File tempFile = File.createTempFile("temp", cdh.getFileName());
		FileUtils.writeByteArrayToFile(tempFile, amrAsBytes);
//		FileUtils.writeByteArrayToFile(tempFile, binaryInput);
//		Files.copy(inputStream, tempFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
		System.out.println("Output file: " + tempFile);


//		EmotionalStateWithText emotionalStateWithText = speech2Text.RunSyncRequest(amrAsBytes);
//		lastSlimEmotionalState = emotionalStateWithText._emotions;
		long now = System.currentTimeMillis();
		if (now > lastTimeForTextToSpeech + 5*1000) {
			lastTimeForTextToSpeech = now;
			File finalTempFile = tempFile;
			new Thread(() -> {
				try {
					Process speech2Text = new ProcessBuilder("java", "-cp", Settings.get(Settings.SPEECH_2_TEXT_TO_EMOTION_JAR), "socialbuddy.ApiResource", "Sync", finalTempFile.getPath())
							.start();
					String speech2TextRes = IOUtils.toString(speech2Text.getInputStream(), StandardCharsets.UTF_8);
					System.out.println("Speech2Text Res:\n" + speech2TextRes);
					int exitStatus = speech2Text.waitFor();
					if (exitStatus != 0) {
						throw new RuntimeException("Speech2Text failed. Status: " + exitStatus + ".\n Res: " + speech2TextRes);
					}
					List<String> lastLines = getLastLines(speech2TextRes, 9);
					lastLines.remove(lastLines.size() - 1); // Delete the empty last line.

					SlimEmotionalState slimEmotionalState = new SlimEmotionalState();
					String line = lastLines.get(5);
					String[] split = line.split(":", 2);
					slimEmotionalState.setFear(Integer.parseInt(split[1].trim()));

					line = lastLines.get(4);
					split = line.split(":", 2);
					slimEmotionalState.setAnger(Integer.parseInt(split[1].trim()));

					line = lastLines.get(3);
					split = line.split(":", 2);
					slimEmotionalState.setSadness(Integer.parseInt(split[1].trim()));

					line = lastLines.get(2);
					split = line.split(":", 2);
					slimEmotionalState.setHappiness(Integer.parseInt(split[1].trim()));

					line = lastLines.get(0);
					slimEmotionalState.setText(line);

					lastSlimEmotionalState = slimEmotionalState;

				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}).start();
		}

		tempFile = convertToWav(tempFile);

		Process vokaturiResProcess = new ProcessBuilder("python3", "measure_wav_linux64.py", tempFile.getPath())
				.redirectErrorStream(true)
				.directory(new File(Settings.get(Settings.VOKATURI_INSTALL_FOLDER)))
				.start();

		String vokaturiRes = IOUtils.toString(vokaturiResProcess.getInputStream(), StandardCharsets.UTF_8);

		System.out.println("Vokaturi Res:\n" + vokaturiRes);

		int exitStatus = vokaturiResProcess.waitFor();
		if (exitStatus != 0) {
			throw new RuntimeException("Vokaturi failed. Status: " + exitStatus + ".\n Res: " + vokaturiRes);
		}

		String lastLine = getLastLines(vokaturiRes, 2).get(0);
		if (lastLine.equals("Not enough sonorancy to determine emotions")) {
			return gson.toJson(new BigResponse().setRealTime(new EmotionState(1d, 0d, 0d, 0d, 0d)));
		}

		List<String> lastLines = getLastLines(vokaturiRes, 6);
		lastLines.remove(lastLines.size() - 1); // Delete the empty last line.

		Map<String, Double> emotions = new HashMap<>();
		lastLines.stream().forEach(line -> {
			String[] split = line.split(":", 2);
			emotions.put(split[0], Double.parseDouble(split[1]));
		});
		BigResponse bigResp = new BigResponse();
		bigResp.setRealTime(gson.fromJson(gson.toJson(emotions), EmotionState.class));
		ResponseMerger.TextToEmotionResponse historyResp = ResponseMerger.getSimpleResponse(lastSlimEmotionalState);
		if (lastSlimEmotionalState != null) {
			lastSlimEmotionalState.setText("");
		}
		bigResp.setHistory(historyResp);
		String response = gson.toJson(bigResp);
		System.out.println(response);
		return response;
	}

	static private File convertToWav(File file) throws IOException, InterruptedException {
		if (com.google.common.io.Files.getFileExtension(file.getName()).equalsIgnoreCase("wav")) {
			return file;
		}

		File newWavFile = new File(file.getParent(), Files.getNameWithoutExtension(file.getName()) + ".wav");
		Process convertProcess = new ProcessBuilder("ffmpeg", "-i", file.getPath(), newWavFile.getPath())
				.redirectErrorStream(true)
//				.directory(new File(Settings.get(Settings.VOKATURI_INSTALL_FOLDER)))
				.start();

		int exitStatus = convertProcess.waitFor();

		if (exitStatus != 0) {
			throw new RuntimeException("Conversion failed. Status: " + exitStatus);
		}

		if (!newWavFile.exists()) {
			throw new RuntimeException("Conversion failed.");
		}

		return newWavFile;
	}

	static private List<String> getLastLines(String string, int numLines) {
		List<String> lines = new ArrayList<>();

		int currentEndOfLine = string.length();
		for (int i = 0; i < numLines; ++i) {
			int lastEndOfLine = currentEndOfLine;
			currentEndOfLine = string.lastIndexOf('\n', lastEndOfLine - 1);
			String lastLine = string.substring(currentEndOfLine + 1, lastEndOfLine);
			lines.add(0, lastLine);
//			System.out.println("Last line: " + lastLine);
		}

		return lines;
	}

	public enum MOO {
		Sync,
		Async,
		Voka,
	}

	public static void main(String[] args) throws Exception {
		if (args.length < 2) {
			System.out.println("Please provide MOO [Sync,Async,Voka] and file/path");
			System.exit(0);
		}

		MOO moo = MOO.valueOf(args[0]);
		String fileName = args[1];

		byte[] bytes;
		Speech2Text speech2Text;

		switch (moo) {
			case Sync:
				bytes = Files.toByteArray(new File(fileName));
				speech2Text = new Speech2Text();
				EmotionalStateWithText emotionalStateWithText = speech2Text.RunSyncRequest(bytes);

				System.out.println("Text:\n" + emotionalStateWithText._emotions.getText());
				System.out.println("Emotions:\n" + emotionalStateWithText._emotions.toString());
				break;
			case Async:
				Consumer<EmotionalStateWithText> observer = new Consumer<EmotionalStateWithText>() {
					@Override
					public void accept(EmotionalStateWithText emotionalStateWithText) {
						System.out.println("Text:\n" + emotionalStateWithText._emotions.getText());
						System.out.println("Emotions:\n" + emotionalStateWithText._emotions.toString());
					}
				};

				bytes = Files.toByteArray(new File(fileName));
				speech2Text = new Speech2Text();
				speech2Text.AddRequest(bytes, observer);
				synchronized (observer) {
					observer.wait();
				}
				break;
			case Voka:
				RunVokaOnDirectory(fileName);
				break;
		}
	}

	private static void RunVokaOnDirectory(String directoryName) throws IOException, InterruptedException {
		File directory = new File(directoryName);

		try (DirectoryStream<java.nio.file.Path> stream = java.nio.file.Files.newDirectoryStream(directory.toPath(), "*.{wav}")) {
			for (java.nio.file.Path entry : stream) {
				File file = entry.toFile();
				System.out.println("Running vokaturi on " + file);

				try {
					Process vokaturiResProcess = new ProcessBuilder("python", "measure_wav_linux64.py", file.getPath())
							.redirectErrorStream(true)
							.directory(new File("c:\\Users\\unicorn\\IdeaProjects\\socialbuddy\\server\\OpenVokaturi-2-1b\\examples\\"))
							.start();

					String vokaturiRes = IOUtils.toString(vokaturiResProcess.getInputStream(), StandardCharsets.UTF_8);

					System.out.println("Vokaturi Res:\n" + vokaturiRes);

					int exitStatus = vokaturiResProcess.waitFor();
				} catch (Exception e) {
					System.err.println(e);
					break;
				}
			}
		} catch (IOException | DirectoryIteratorException x) {
			// IOException can never be thrown by the iteration.
			// In this snippet, it can only be thrown by newDirectoryStream.
			System.err.println(x);
		}
	}

/*	public static void main(String[] args) throws Exception {
		process(new FileInputStream("/home/noa/Downloads/test2.amr"), new FormDataContentDisposition("form-data;name=aaa;filename=\"test2.amr\";", true));
        boolean x = true;
        while(x){
            if(!x){
                break;
            }
        }
        process(new FileInputStream("/home/noa/Downloads/test2.amr"), new FormDataContentDisposition("form-data;name=aaa;filename=\"test2.amr\";", true));
		while(x){
			if(!x){
				break;
			}
		}
		process(new FileInputStream("/home/noa/Downloads/test2.amr"), new FormDataContentDisposition("form-data;name=aaa;filename=\"test2.amr\";", true));
	}*/
}

