package socialbuddy.synesketch.app;

import socialbuddy.synesketch.emotion.EmotionalState;

/**
 * Contains emotional state with minimal properties
 *
 * Created by chaiavi on 3/27/2017.
 */
public class SlimEmotionalState {
  private String text;
  private int Happiness;
  private int Sadness;
  private int Anger;
  private int Fear;
  private int Disgust;
  private int Surprise;

  public SlimEmotionalState() {

  }

  public SlimEmotionalState(EmotionalState emotionalState) {
    setAnger(Double.valueOf(emotionalState.getAngerWeight() * 100).intValue());
    setDisgust(Double.valueOf(emotionalState.getDisgustWeight() * 100).intValue());
    setFear(Double.valueOf(emotionalState.getFearWeight() * 100).intValue());
    setHappiness(Double.valueOf(emotionalState.getHappinessWeight() * 100).intValue());
    setSadness(Double.valueOf(emotionalState.getSadnessWeight() * 100).intValue());
    setSurprise(Double.valueOf(emotionalState.getSurpriseWeight() * 100).intValue());

    setText(emotionalState.getText());
  }

  public int getHappiness() {
    return Happiness;
  }

  public void setHappiness(int happiness) {
    Happiness = happiness;
  }

  public int getSadness() {
    return Sadness;
  }

  public void setSadness(int sadness) {
    Sadness = sadness;
  }

  public int getAnger() {
    return Anger;
  }

  public void setAnger(int anger) {
    Anger = anger;
  }

  public int getFear() {
    return Fear;
  }

  public void setFear(int fear) {
    Fear = fear;
  }

  public int getDisgust() {
    return Disgust;
  }

  public void setDisgust(int disgust) {
    Disgust = disgust;
  }

  public int getSurprise() {
    return Surprise;
  }

  public void setSurprise(int surprise) {
    Surprise = surprise;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();

    sb.append("Happiness: " + getHappiness());
    sb.append("\nSadness: " + getSadness());
    sb.append("\nAnger: " + getAnger());
    sb.append("\nFear: " + getFear());
    sb.append("\nDisgust: " + getDisgust());
    sb.append("\nSurprise: " + getSurprise());

    return sb.toString();
  }
}
