package socialbuddy.synesketch.app;

import socialbuddy.synesketch.emotion.EmotionalState;
import socialbuddy.synesketch.emotion.Empathyscope;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by chaiavi on 3/26/2017.
 */
public class TextToEmotion {
  private static final String FILENAME = "emotions.txt";

  public static void main(String[] args ) {
    if (args.length < 1) {
      System.out.println("Please send an argument with the string you want to sense for emotion");
      System.exit(0);
    }
    String text = args[0];

    System.out.println(text2Emotion(text));
  }

  /** Returns a list of emotions stats from any given text */
  public static SlimEmotionalState text2Emotion(String text) {
    SlimEmotionalState slimEmotionalState = new SlimEmotionalState();
    try {
      EmotionalState sentenceState = Empathyscope.getInstance().feel(text);
      slimEmotionalState = new SlimEmotionalState(sentenceState);
    } catch (IOException e) {
      e.printStackTrace();
    }

    return slimEmotionalState;
  }
}
