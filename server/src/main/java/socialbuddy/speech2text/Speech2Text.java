/*
  Copyright 2017, Google Inc.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package socialbuddy.speech2text;

import com.google.api.gax.grpc.OperationFuture;
import com.google.cloud.speech.spi.v1beta1.SpeechClient;
import com.google.cloud.speech.v1beta1.*;
import com.google.cloud.speech.v1beta1.RecognitionConfig.AudioEncoding;
import com.google.protobuf.ByteString;
import socialbuddy.synesketch.app.SlimEmotionalState;
import socialbuddy.synesketch.app.TextToEmotion;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public class Speech2Text implements AutoCloseable {
    private SpeechClient _speech;
    private final RecognitionConfig _recognitionConfig;
    private final List<OperationFuture<AsyncRecognizeResponse>> _futureList = new ArrayList<>();

    public Speech2Text() throws Exception {       // Instantiates a client
//        _speech = SpeechClient.create();

        // Builds the sync recognize request
        _recognitionConfig = RecognitionConfig.newBuilder()
                .setEncoding(AudioEncoding.AMR)
                .setSampleRate(8000)
                .setMaxAlternatives(1)
                .build();
    }

    public EmotionalStateWithText RunSyncRequest(byte[] data) throws Exception {
        ByteString audioBytes = ByteString.copyFrom(data);

        RecognitionAudio audio = RecognitionAudio.newBuilder()
                .setContent(audioBytes)
                .build();

        SpeechClient _speech = SpeechClient.create();
        try {
            SyncRecognizeResponse syncResponse = _speech.syncRecognize(_recognitionConfig, audio);
            List<SpeechRecognitionResult> resultsList = syncResponse.getResultsList();

            List<SpeechRecognitionAlternative> speechRecognitionAlternatives = GetBestRecognizedText(resultsList);
            SpeechRecognitionAlternative bestAlternative = speechRecognitionAlternatives.isEmpty() ? null : speechRecognitionAlternatives.get(0);
            return GetEmotionState(bestAlternative);
//            OperationFuture<AsyncRecognizeResponse> future = _speech.asyncRecognizeAsync(_recognitionConfig, audio);
//
//            AsyncRecognizeResponse asyncRecognizeResponse = future.get();
//
//            List<SpeechRecognitionResult> results = asyncRecognizeResponse.getResultsList();
//
//            List<SpeechRecognitionAlternative> speechRecognitionAlternatives = Speech2Text.GetBestRecognizedText(results);
//            SpeechRecognitionAlternative speechRecognitionAlternative = speechRecognitionAlternatives.isEmpty() ? null : speechRecognitionAlternatives.get(0);
//            EmotionalStateWithText emotionalStateWithText = Speech2Text.GetEmotionState(speechRecognitionAlternative);
//            return emotionalStateWithText;
        } finally {
            _speech.close();
        }
    }

    public static EmotionalStateWithText GetEmotionState(SpeechRecognitionAlternative bestAlternative) {
        EmotionalStateWithText data = new EmotionalStateWithText();
        data._confidence = 0;
        data._emotions = new SlimEmotionalState();

        if ( bestAlternative != null ) {
            data._confidence = bestAlternative.getConfidence();
            data._emotions = TextToEmotion.text2Emotion(bestAlternative.getTranscript());
        }

        return data;
    }

    public static List<SpeechRecognitionAlternative> GetBestRecognizedText(List<SpeechRecognitionResult> resultsList) {
        List<SpeechRecognitionAlternative> listBestAlternative = new ArrayList<>();

        for (SpeechRecognitionResult result : resultsList) {
            List<SpeechRecognitionAlternative> alternatives = result.getAlternativesList();

            SpeechRecognitionAlternative bestAlternative = null;

            for (SpeechRecognitionAlternative alternative : alternatives) {
                if (bestAlternative == null || alternative.getConfidence() > bestAlternative.getConfidence()) {
                    bestAlternative = alternative;
                }
            }

            listBestAlternative.add(bestAlternative);
        }

        return listBestAlternative;
    }

    public void AddRequest(byte[] data, Consumer<EmotionalStateWithText> observer) throws ExecutionException, InterruptedException {
        ByteString audioBytes = ByteString.copyFrom(data);

        RecognitionAudio audio = RecognitionAudio.newBuilder()
                .setContent(audioBytes)
                .build();

        // Send async request
        OperationFuture<AsyncRecognizeResponse> future = _speech.asyncRecognizeAsync(_recognitionConfig, audio);

        // Add listener to the future
         future.addListener(new SpeechRecognitionTask(this, future, observer), Executors.newSingleThreadExecutor());
        synchronized(_futureList) {
            _futureList.add(future);
        }
    }

    public void close() throws Exception {
        synchronized (_futureList) {
            for (OperationFuture<AsyncRecognizeResponse> future : _futureList) {
                if (!future.isDone()) {
                    System.out.println("Waiting 5 secnods for future");
                    future.awaitAsyncCompletion(5, TimeUnit.SECONDS);
                    if (!future.isDone()) {
                        System.out.println("Future cancel");
                        future.cancel(true);
                    }
                }
            }
        }

        if (_speech != null)
            _speech.close();
    }

    public void RemoveFuture(OperationFuture<AsyncRecognizeResponse> future) {
        synchronized(_futureList) {
            _futureList.remove(future);
        }
    }
}
