package socialbuddy.speech2text;

import com.google.api.gax.grpc.OperationFuture;
import com.google.cloud.speech.v1beta1.AsyncRecognizeResponse;
import com.google.cloud.speech.v1beta1.SpeechRecognitionAlternative;
import com.google.cloud.speech.v1beta1.SpeechRecognitionResult;
import com.google.common.util.concurrent.FutureCallback;

import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.Callable;
import java.util.function.Consumer;

import javax.annotation.Nullable;

import socialbuddy.synesketch.app.TextToEmotion;

/**
 * Created by chaiavi on 3/28/2017.
 */
public class SpeechRecognitionTask implements Runnable, FutureCallback<AsyncRecognizeResponse> {
	private final Consumer<EmotionalStateWithText> _observer;
    private final Speech2Text _parent;
    private final OperationFuture<AsyncRecognizeResponse> _future;

	SpeechRecognitionTask(Speech2Text parent, OperationFuture<AsyncRecognizeResponse> future, Consumer<EmotionalStateWithText> observer) {
        _parent = parent;
        _future = future;
        _observer = observer;
    }

	public void run() {
		try {
			AsyncRecognizeResponse asyncRecognizeResponse = _future.get();
			run(asyncRecognizeResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void run(AsyncRecognizeResponse asyncRecognizeResponse) {
        List<SpeechRecognitionResult> results = asyncRecognizeResponse.getResultsList();

        List<SpeechRecognitionAlternative> speechRecognitionAlternatives = Speech2Text.GetBestRecognizedText(results);
        SpeechRecognitionAlternative speechRecognitionAlternative = speechRecognitionAlternatives.isEmpty() ? null : speechRecognitionAlternatives.get(0);
        EmotionalStateWithText emotionalStateWithText = Speech2Text.GetEmotionState(speechRecognitionAlternative);

        _observer.accept(emotionalStateWithText);
        _parent.RemoveFuture(this._future);
    }

	@Override
	public void onSuccess(@Nullable AsyncRecognizeResponse asyncRecognizeResponse) {
		run(asyncRecognizeResponse);
	}

	@Override
	public void onFailure(Throwable throwable) {
		throw new RuntimeException(throwable);
	}
}
