package socialbuddy;

import java.util.HashMap;

/**
 * Created by alik on 3/27/17.
 */
public class Settings {

	public static final String VOKATURI_INSTALL_FOLDER = "vokatory.install.folder";
	public static final String SPEECH_2_TEXT_TO_EMOTION_JAR = "speech2text2emotion.jar.location";
	private static HashMap<String, String> SETTINGS = new HashMap<>();


	public static void set(HashMap<String, String> settings) {
		SETTINGS = settings;
	}

	public static String get(String key) {
		return SETTINGS.get(key);
	}
}
