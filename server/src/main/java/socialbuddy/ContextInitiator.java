package socialbuddy;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * Created by alik on 3/27/17.
 */
public class ContextInitiator implements Filter {

	public void init(FilterConfig config) throws ServletException {
//		new ResourceConfig()
//				.packages("org.glassfish.jersey.examples.multipart")
//				.register(MultiPartFeature.class);

		InputStream configPropsAsStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("config.properties");
		try {
			Properties properties = new Properties();
			properties.load(configPropsAsStream);
			Settings.set(new HashMap(properties));
		} catch (IOException e) {
			throw new RuntimeException(e);
		} finally {
			IOUtils.closeQuietly(configPropsAsStream);
		}
	}

	public void destroy() {
	}

	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
		chain.doFilter(req, resp);
	}


}
