package socialbuddy;
import java.util.*;

/**
 * Created by noa on 28/03/17.
 */
public class EmotionResponse {
    private String text;
    private Map<Emotion, Double> emotionPercentage = new HashMap<>();

    public EmotionResponse() {

    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "EmotionResponse{" +
                "text='" + text + '\'' +
                ", emotionPercentage=" + emotionPercentage +
                '}';
    }

    public void addPercentageEntry(Emotion emotion, Double value, boolean shouldDivide) {
            Double existingPercentage = emotionPercentage.containsKey(emotion) ? emotionPercentage.get(emotion) : 0.0;
            if(shouldDivide){
                emotionPercentage.put(emotion, (existingPercentage + value)/2); //todo: change if we have more integrations
            }else {
                emotionPercentage.put(emotion, value);
            }
    }
    public Emotion getBestEmotion() {
        Emotion bestEmotion = null;
        double max = 0.0;
        for(Map.Entry<Emotion, Double> entry : emotionPercentage.entrySet()){
            if(entry.getValue() > max){
                bestEmotion = entry.getKey();
                max = entry.getValue();
            }
        }
        return bestEmotion;
    }


    public enum Emotion {
        HAPPY("happy", "happiness"),
        SAD("sad", "sadness"),
        ANGRY("angry", "anger"),
        FEAR("fear"),
        DISGUST("disgust"),
        SURPRISE("surprise"),
        NEUTRAL("neutral");

        private Set<String> validStrings;

        Emotion(String... surprise) {
            Set<String> stringSet = new HashSet<>();
            Collections.addAll(stringSet, surprise);
            this.validStrings = stringSet;
        }

        public String getName(){
             return this.name().substring(0,1).toUpperCase() + this.name().substring(1).toLowerCase();
        }

        public static Emotion getEmotion(String emotionString){
            for(Emotion emotion: Emotion.values()){
                if(emotion.validStrings.contains(emotionString.toLowerCase())){
                    return emotion;
                }
            }
            return null;
        }
    }

}
