package socialbuddy;

/**
 * Created by alik on 3/29/17.
 */
public class EmotionState {
	public Double Neutral;
	public Double Fear;
	public Double Happy;
	public Double Angry;
	public Double Sad;


	/**
	 * For deserialization
	 */
	public EmotionState() {
	}

	public EmotionState(Double neutral, Double fear, Double happy, Double angry, Double sad) {
		Neutral = neutral;
		Fear = fear;
		Happy = happy;
		Angry = angry;
		Sad = sad;
	}
}
