package socialbuddy;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import socialbuddy.synesketch.app.SlimEmotionalState;
import java.util.HashMap;
import java.util.Map;


public class ResponseMerger{


    //gets a json string of voiceToEmotion response and SlimEmotionalState from textToEmotion response
    //and returns a json response of most significant emotion and text
    public static String createMergedResponse(String voiceToEmo, SlimEmotionalState textToEmo){
        Gson gson = new Gson();
        String textToJson = gson.toJson(textToEmo);
        @SuppressWarnings("unchecked") Map<String, Object> textMap = gson.fromJson(textToJson, Map.class);
        String text = (String) textMap.get("text");
        Map<String, Double> normalizedMap = getNormalizedMap(textMap);
        @SuppressWarnings("unchecked") Map<String, String> voiceMap = gson.fromJson(voiceToEmo, Map.class);
        String bestEmotion = mergeMaps(voiceMap, normalizedMap);
        TextToEmotionResponse textToEmotionResponse = new TextToEmotionResponse(bestEmotion, text);
        return gson.toJson(textToEmotionResponse);

    }

    public static TextToEmotionResponse getSimpleResponse(SlimEmotionalState textToEmo){
        if (textToEmo == null) {
            return null;
        }

        Gson gson =  new GsonBuilder()
                .setPrettyPrinting()
                .disableHtmlEscaping()
                .create();
        String textToJson = gson.toJson(textToEmo);
        @SuppressWarnings("unchecked") Map<String, Object> textMap = gson.fromJson(textToJson, Map.class);
        String text = textToEmo.getText();
        Map<String, Double> normalizedMap = getNormalizedMap(textMap);
        EmotionResponse emotionResponse = new EmotionResponse();
        for(Map.Entry<String, Double> entry : normalizedMap.entrySet()){
            EmotionResponse.Emotion emotion = EmotionResponse.Emotion.getEmotion(entry.getKey());
            emotionResponse.addPercentageEntry(emotion, entry.getValue(), false);
        }
        EmotionResponse.Emotion bestEmotion = emotionResponse.getBestEmotion();
        if (bestEmotion == null) {
            bestEmotion = EmotionResponse.Emotion.NEUTRAL;
        };
        TextToEmotionResponse textToEmotionResponse = new TextToEmotionResponse(bestEmotion.getName(), text);
        return textToEmotionResponse;
    }

    private static String mergeMaps(Map<String, String> voiceMap, Map<String, Double> normalizedMap) {
        EmotionResponse emotionResponse = new EmotionResponse();
        for(Map.Entry<String, Double> entry : normalizedMap.entrySet()){
            EmotionResponse.Emotion emotion = EmotionResponse.Emotion.getEmotion(entry.getKey());
            emotionResponse.addPercentageEntry(emotion, entry.getValue(), true);
        }
        for(Map.Entry<String, String> entry : voiceMap.entrySet()){
            EmotionResponse.Emotion emotion = EmotionResponse.Emotion.getEmotion(entry.getKey());
            emotionResponse.addPercentageEntry(emotion, Double.valueOf(entry.getValue()), true);
        }

        return emotionResponse.getBestEmotion().name();
    }

    private static Map<String, Double> getNormalizedMap(Map<String, Object> textMap) {
        Map<String, Double> ret = new HashMap<>();
        double sum = 0;
        textMap.remove("text");
        for(Object doubleString : textMap.values()){
            sum +=  (Double) doubleString;
        }
        for(Map.Entry<String, Object> entry : textMap.entrySet()){
            Double aDouble = (Double)(entry.getValue());
            double value = aDouble / sum;
            ret.put(entry.getKey(), value);
        }
        return ret;
    }


    public static class TextToEmotionResponse {
        private String emotion;
        private String text;

        public TextToEmotionResponse(String emotion, String text) {
            this.emotion = emotion;
            this.text = text;
        }

        public String getEmotion() {
            return emotion;
        }

        public void setEmotion(String emotion) {
            this.emotion = emotion;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }
    }
}
