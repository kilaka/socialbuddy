﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397704
// To debug code on page load in Ripple or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.
(function () {
    "use strict";

    // -------   constants   -------------------- //
    var INTERVAL_TIME = 3000;
    var MIN_EMOTION_LEVEL = 0.4;
    var MIN_SECONDARY_LEVEL = 0.35;
    var NEUTRAL_IMAGE = 'Neutral';

    var src = "myrecording.amr";
    var mediaRec = null;
    var file_path = "";
    // -------   end of constants   ------------ //

    document.addEventListener('deviceready', onDeviceReady.bind(this), false);
    var _emotions = ['Neutral', 'Fear', 'Happy', 'Angry', 'Sad', 'Surprise', 'Disgust']; 
    var _emotionImages = {};
    var _captureTimer;
    var recording = false;

    function onDeviceReady() {
        // Handle the Cordova pause and resume events
        //document.addEventListener( 'pause', onPause.bind( this ), false );
        //document.addEventListener( 'resume', onResume.bind( this ), false );

        // TODO: Cordova has been loaded. Perform any initialization that requires Cordova here.
        //var parentElement = document.getElementById('deviceready');
        //var listeningElement = parentElement.querySelector('.listening');
        //var receivedElement = parentElement.querySelector('.received');
        //listeningElement.setAttribute('style', 'display:none;');
        //receivedElement.setAttribute('style', 'display:block;');

        createImageDictionary();
        $('#initButton').click(captureAudio);

        mediaRec = new Media(src, onSuccess, onError);
        file_path = cordova.file.externalRootDirectory + src;

        console.log(mediaRec);
        console.log(file_path);
        console.log(FileTransfer);
    };

    function createImageDictionary() {
        var path = 'images/';
        var extension = '.png';
        for (var i = 0; i < _emotions.length; i++) {
            _emotionImages[_emotions[i]] = path + _emotions[i] + extension;
        }
    }

    function getMockData() {
        var distribution = [];
        var sum = 0;
        var result = {};

        for (var i = 0; i < _emotions.length; i++) {
            var part = Math.pow(Math.random(), 3);
            distribution[i] = part;
            sum += part;
        };

        for (var i = 0; i < _emotions.length; i++) {
            result[_emotions[i]] = distribution[i] / sum;
        }

        return result;
    }

    function rankCompareDesc(a, b) {
        if (a.rank < b.rank) {
            return 1;
        } else if (a.rank > b.rank) {
            return -1;
        } else {
            return 0;
        }
    }

    function createSortedArray(obj) {
        var result = [];
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) {
                result.push({ name: attr, rank: obj[attr] });
            }
        }

        return result.sort(rankCompareDesc);
    }

    function setEmotions(distribution) {
        var arr = createSortedArray(distribution);
        var primary;
        var secondary;

        if (arr[0].rank < MIN_EMOTION_LEVEL) {//no dominant emotion
            primary = NEUTRAL_IMAGE;
            secondary = arr[0].rank < MIN_SECONDARY_LEVEL || arr[0].name === NEUTRAL_IMAGE ? null : arr[0].name;
        }
        else {
            primary = arr[0].name;
            secondary = arr[1].rank < MIN_SECONDARY_LEVEL ? null : arr[1].name;
        }

        setEmotionImages(primary, secondary);
    }

    function setButtonText(text) {
        var btn = $('#initButton');
        btn.attr('value', text);
        if (text === 'Start') {
            btn.addClass('btn-success');
            btn.removeClass('btn-danger');
        } else {
            btn.addClass('btn-danger');
            btn.removeClass('btn-success');
        }
    }

    function setEmotionImages(primaryImage, secondaryImage) {
        var primaryId = 'emotionImage';
        var secondaryId = 'secondaryEmotionImage';

        setElementImage(primaryId, primaryImage);
        setElementImage(secondaryId, secondaryImage);
    }

    function setElementImage(elementId, img) {
        var element = $('#' + elementId);
        if (img) {
            element.attr('src', _emotionImages[img]);
            element.removeClass('hidden');
        } else {
            element.attr('src', '');
            element.addClass('hidden');
        }
    }

    function getMockText() {
        var st = [
            'Lorem ipsum dolor sit amet',
            'consectetur adipisicing elit.',
            'Eum quis velit temporibus cupiditate laborum sapiente',
            'animi dolores sit fuga',
            'officia aliquam nisi ducimus quibusdam?',
            'Vel, illum accusamus',
            'laudantium animi explicabo!'
        ];

        return {
            text: st[Math.floor(Math.random() * st.length)],
            emotion: _emotions[Math.floor(Math.random() * _emotions.length)]
        }
    }

    function getHistoryContainer() {
        return $('#historyContainer');
    }

    function getHistoryScrollArea() {
        return $('#scrollArea');
    }

    function addBlock(data) {
        if (!data || !data.text || data.text === '') return;
        var scrollArea = getHistoryScrollArea();
        var container = getHistoryContainer();
        var image = _emotionImages[data.emotion];
        var block = '<div class="block">[' + data.text + ']<img src="' + image + '"/></div>'
        scrollArea.append(block);
        container.animate({ scrollLeft: scrollArea.outerWidth() }, '2000');
    }

    function clearHistory() {
        getHistoryScrollArea().empty();
    }

    function captureAudio() {
      
        if (_captureTimer)
        {
            clearInterval(_captureTimer);
            _captureTimer = null;
            setButtonText('Start');
            //setEmotionImages('', '');
            mediaRec.stopRecord();

        }
        else {
            _captureTimer = setInterval(function () {
                //voice to emotion
                mediaRec.stopRecord();
                uploadFile();
                mediaRec = new Media(src, onSuccess, onError);
                mediaRec.startRecord();                
                //setEmotions(getMockData());


            }, INTERVAL_TIME);

            setButtonText('Stop');
            mediaRec = new Media(src, onSuccess, onError);
            mediaRec.startRecord();
            clearHistory();
        }
    }

    function uploadFile() {

        var serverUrl = "http://35.162.156.142:8080/server/api/process";

        var options = new FileUploadOptions();
        options.fileKey = "file";
        options.fileName = file_path.substr(file_path.lastIndexOf('/') + 1);
        options.mimeType = "audio/amr";
        options.chunkedMode = false;
         
        // respons reply from server exp
        //{bytesSent: 11635, 
        // responseCode: 200, 
        //response: "{"Neutral":0.896,"Fear":0.0,"Happy":0.008,"Angry":0.096,"Sad":0.0}", objectId: ""}

        var ft = new FileTransfer();
        ft.upload(file_path, encodeURI(serverUrl), function (r) {
            //voice to emotion
            var data = JSON.parse(r.response);
            setEmotions(data.realTime);
            //voice to text + emotion
            addBlock(data.history);
            //addBlock(getMockText());
            console.log("win");
            console.log(r);
        }, function (error) {
            console.log("fail");
            console.log(error);
        }, options);
    }
    
    // onSuccess Callback   
    function onSuccess() {
        console.log("recordAudio():Audio Success");
    }

    // onError Callback   
    function onError(error) {
        console.log("recordAudio():Audio error");
        console.log(error);
    }
 
 
})();